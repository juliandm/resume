{
    "$schema": "https://json.schemastore.org/resume",
    "basics": {
      "name": "Julian de Mourgues",
      "label": "AWS Cloud Consultant at PROTOS Technologie",
      "picture": "https://public-images-juliandemourgues.s3.eu-central-1.amazonaws.com/profile.jpg",
      "email": "juliandemourgues@edurata.com",
      "phone": "+4915202787043",
      "url": "https://juliandemourgues.com",
      "summary": "Cloud Solutions Architect (AWS, GCP) specialised on containerised microservice architectures with Kubernetes. Previous projects mainly for corporations in automotive industry (Daimler, Bosch, Bosch Engineering, Capgemini). Native german speaker working from Berlin and currently (still) available.",
      "location": {
        "address": "Bessemerstraße 82",
        "postalCode": "12103",
        "city": "Berlin, Germany",
        "countryCode": "DE",
        "region": "Berlin"
      },
      "profiles": [
        {
          "network": "LinkedIn",
          "username": "Julian de Mourgues",
          "url": "https://www.linkedin.com/in/juliandm/",
          "color": "#006192"
        },
        {
          "network": "StackOverflow",
          "username": "Julian de Mourgues",
          "url": "https://stackoverflow.com/users/7457239/julian-dm",
          "color": "#f48024"
        },
        {
          "network": "GitHub",
          "username": "juliandm",
          "url": "https://github.com/juliandm",
          "color": "#24292e"
        },
        {
          "network": "Medium",
          "username": "juliandm",
          "url": "https://juliandm.medium.com/",
          "color": "#000000"
        },
        {
          "network": "Email",
          "username": "juliandemourgues",
          "url": "mailto:juliandemourgues@edurata.com",
          "color": "#000000"
        }
      ]
    },
    "work": [
      {
        "name": "Rackspace & Big Automotive company",
        "highlights": [
          "General system team to support in all cloud and AWS related tasks as Devops Engineer and Architect",
          "Self governing with JIRA, was making initiative on how to organise the team of 3 on JIRA",
          "Designing solution for high performance BI applications with Aurora and DocumentDB",
          "VPC incloud migration planning and execution with multiple stakeholders and consumers (databases, lambdas, SQS, step functions, etc..)",
          "On-prem to cloud migration planning and advisory of whole team architecture",
          "Containerised images with linux and Python",
          "User Management with Azure AD and customer-developed authentication service (PPN)",
          "Implementation of SSO authentication flow for API Gateway"
        ],
        "technologies": [
          "Terraform", "Terragrunt", "Azure AD", "OIDC", "OpenTofu","AWS Glue", "AWS CDK", "AWS", "Prisma Alerts", "Aurora RDS", "Budget", "OIDC", "SonarQube", "Linux", "Python"
        ],
        "position": "Terraform expert",
        "startDate": "2023-05-01",
        "endDate": "2024-12-31",
        "summary": "Joined in order to share knowledge on terraform and devops and support"
      },
      {
        "name": "Peek & Cloppenburg Group",
        "highlights": [
          "IT subbranch of big german fashion retailer and placed in a 8 head scrum team of a cloud based team of cloud architects and security engineers",
          "mainly terraform tasks and knowledge transfer",
          "onboarding tasks for colleagues in terraform and cloud architecture",
          "Working in Kanban and Scrum",
          "Containerised images with linux and Javascript"
        ],
        "technologies": [
          "Terraform", "Terragrunt", "Pulumi", "GCP", "github actions", "github", "GCP authentication", "GCP identity pool", "GCP networking", "SonarQube", "Gitlab CI", "Docker", "Javascript"
        ],
        "position": "Terraform expert",
        "startDate": "2022-12-01",
        "endDate": "2023-04-01",
        "summary": "Joined in order to share knowledge on terraform and devops and support"
      },
      {
        "name": "Catella Residential Investment Management GmbH",
        "highlights": [
          "Smaller project with 2 BI developers, 1 Product owner, me as solitary devops",
          "Design and implementation of whole Devops architecture to build & deploy applications (ecs tasks) on github and AWS",
          "security compliant to ISO 27001",
          "Working in Kanban and Scrum",
          "AWS SSO and AWS Secrets manager for security"
        ],
        "technologies": [
          "Terraform", "OpenTofu", "AWS", "AWS SSO", "EKS", "ECS", "github actions", "github", "AWS Fargate", "AWS Secrets manager", "Datadog", "SonarQube", "Gitlab CI", "AWS MKS (Kafka)", "ISO 27001"
        ],
        "position": "Devops Consultant",
        "startDate": "2022-04-01",
        "endDate": "2023-02-01",
        "summary": "Architecting and implementing a Green field project of BI on AWS"
      },
      {
        "name": "RTL Data",
        "highlights": [
          "Countless pipelines with gitlab",
          "helm deployment of kubernetes applications in pipeline",
          "argocd and flux deployment through git",
          "Using k9s and kubernetes lens",
          "Setting up and rolling out argocd for 10+ product teams",
          "Setting up and rolling out airflow for 10+ product teams",
          "Presentation and knowledge sharing through comprehensible graphics & confluence",
          "Operations with kubectl",
          "Implementation of real-time messaging solutions with Kafka/MSK",
          "architecture of gitlab templates and other repositories",
          "Helping to set up EMR Operators, Redshift Operators and other Airflow Operators",
          "Debugging and extending airflow code and DAGS",
          "Working in Kanban and Scrum",
          "Authentication with Microsoft AD (Kerberos)",
          "Refactoring shell scripts in Ansible and then python",
          "CLI development with python framework click"
        ],
        "technologies": [
          "Terraform", "ArgoCd","k9s", "Flux", "AWS EMR", "Microsoft AD", "AWS Redshift", "Apache Airflow", "Kubernetes GKE", "Hashicorp Vault", "Google Cloud", "Helm", "Grafana", "Prometheus", "SonarQube", "Gitlab CI", "Docker", "Python", "AWS SQS", "AWS MKS (Kafka)"
        ],
        "position": "Kubernetes Consultant",
        "startDate": "2021-07-01",
        "endDate": "2022-08-01",
        "summary": "Ongoing DevOps work, mainly support and feature request on terraform & airflow"
      },
      {
        "name": "Auxmoney GmbH",
        "highlights": [
          "Setting up an EKS cluster with RBAC, Security and Configuration Management",
          "Mob Programming (7h per day in a call)",
          "Writing of AWS IAM rules",
          "Writing of Terraform modules and Kubernetes Config Files",
          "Git Ops Approach with Gitlab Pipelines",
          "Working in Kanban and Scrum"
        ],
        "technologies": [
          "AWS EKS", "Fargate", "NodeGroups", "Cluster AutoScaler", "Terraform", "Kubernetes", "Helm", "Sealed Secrets", "Gitlab Pipelines"
        ],
        "position": "Kubernetes Consultant",
        "startDate": "2021-05-01",
        "endDate": "2021-06-31",
        "summary": "Architecting and implementing a Green field project of Kubernetes on AWS"
      },
      {
        "name": "Hansemerkur",
        "highlights": [
            "Writing policies in calico and kubernetes syntax",
            "Custom components of enterprise version tigera calico: tiers, host protection etc.",
            "Writing of helm charts",
            "Guiding and supporting kubernetes migration from cluster to cluster",
            "internal training sessions for collegues for deployment process of K8s Resources onto cluster with gitlab pipelines",
            "Working in Kanban and Scrum",
            "Ansible together with gitlab for more complex pipelines"
        ],
        "technologies": [
          "ArgoCd", "Kibana", "Ansible", "ElasticSearch", "Calico", "Kubernetes", "Helm", "Gitlab Pipelines"
        ],
        "position": "Kubernetes Consultant",
        "startDate": "2020-12-01",
        "endDate": "2021-02-31",
        "summary": "Architecting and implementing a security network through the CNI Calico",
        "url": "https://www.protos-technologie.de"
      },
      {
        "name": "PROTOS Technologie GmbH",
        "highlights": [
            "Responsibility over internal project, PO role towards offshore freelancers",
            "Sole Frontend Developer: Spec, Implementation & automated Testing by me",
            "Complicated flow logic (acyclic directed graph) for ordering, skipping & grouping questions",
            "Hosting of static assets (JS, CSS, HTML) in S3",
            "Whole mutlicloud architecture deployed with terraform",
            "Working in Kanban and Scrum"
        ],
        "technologies": [
          "Git", "S3", "Lambda", "Golang", "Api Gateway", "React", "Terraform", "Jest", "Mocka", "Gitlab CI", "Github actions"
        ],
        "position": "Full-Stack Developer",
        "startDate": "2020-05-31",
        "endDate": "2021-03-31",
        "summary": "Survey tool to determine cloud-readiness of potential customers",
        "url": "https://cloudadvisor.protos-cloudsolutions.de/"
      },
      {
        "name": "PROTOS Technologie GmbH",
        "highlights": [
          "Technical deep dive into Kubernetes and its open source landscape (CI, Monitoring, Storage, CNI's)",
          "Preparation for the Kubernetes Administrator CKA",
          "Performing technical tests and corresponding proof of concept",
          "Categorization of the application according to its optimization potential"
        ],
        "technologies": [
          "Git", "Kubernetes", "Docker", "Prometheus", "SQS"
        ],
        "position": "Cloud Consultant",
        "startDate": "2020-07-01",
        "endDate": "2020-08-31",
        "summary": "Technical Due Diligence for Kubernetes Startup",
        "url": "https://www.protos-technologie.de"
      },
      {
        "name": "Edurata",
        "highlights": [
            "Complete design, idea, development and marketing",
            "Self-organised task tracking with Trello",
            "Offloading selected workload to offshore freelancers (Philippines) with security measures in mind (IAM Roles, Right Handling of Access Keys). Writing concise specifications with fair contractual requirements.",
            "Authorization and User Pooling with Cognito + AWS Amplify + AWS SSO",
            "Data modelling for basic application Types with AppSync (i.e. User, UserSettings, etc..)",
            "Core UVP: Flexible Data structure based on a combination of triplestore (Neptune) for semantics, a key-value store (DynamoDb) for links and per-user S3 Buckets for persistance",
            "Hosting with S3, CloudFront & DNS (Route53) & formerly GCP Compute",
            "API Gateway + Lambda (Python)",
            "DynamoDb & Postgres as databases",
            "Writing custom Terraform provider in GOlang",
            "CI Pipeline with CodeBuild & AWS CDK (CloudFormation)",
            "DevOps tasks with end-to-end operational responsibility",
            "Ansible automation of small scripts",
            ""
        ],
        "technologies": [
          "Git", "CodeDeploy", "Node.js", "GOlang", "Ansible", "CodeBuild","CodePipeline", "Docker", "AWS SQS", "Terraform", "AWS AppSync (Graphql)", "AWS Amplify", "AWS SAM", "Lambda", "GCP CloudRun", "GCP Compute", "Api Gateway", "AWS Cognito", "AWS IAM", "DynamoDB", "React", "Jest"
        ],
        "position": "Senior Architect",
        "startDate": "2019-07-31",
        "endDate": "",
        "summary": "Lightweight Data Warehousing & BI in the cloud on top of a datalake",
        "url": ""
      },
      {
        "name": "Bosch",
        "endDate": "2020-05-31",
        "highlights": [
            "IoT-Application (C++) to demonstrate an automotive com-protocol (SOME/IP)",
            "Implementation on 2 Raspberry Pi’s with custom Linux, one switch and a Laptop for remote access",
            "Testing environment (without Raspberries) through virtual networks/ethernet bridging inside laptop",
            "Consulting of participants and presentation of results in online & on-site for AEF (agricultural lobby)",
            "Writing Unit and integration tests."
        ],
        "technologies": [
          "Git", "Wireshark", "GnuPlot", "C++", "Linux", "Network Tap/bridging", "Custom layer 6 protocol (SOME/IP)", "KVM (Virtual Machine)", "CMake (Compiler)", "Proxying (Bosch Network)"
        ],
        "position": "Technical Researcher & Consultant",
        "startDate": "2019-12-01",
        "summary": "Researching service-orientation in automotive systems. Building a demonstrator based on two raspberries with Some/IP and Adaptive Autosar.",
        "url": "https://bosch.com"
      },
      {
        "name": "Capgemini",
        "endDate": "2019-11-30",
        "highlights": [
          "Built on JSF(JAVA Framework), testing with JUnit, Mockito & PowerMock",
          "CLI Pipeline with Jenkins: Feature + Development (with automatic Testing) + Release Branch + Master (not used).",
          "Part of 6-head Development SCRUM Team. 3 Dev Teams, 1 DevOps, 1 Maintenance",
          "Every three Sprints change towards DevOps and Maintenance Team",
          "CI Pipelining of SQL Db with Liquibase",
          "Kibana/ELK to hotfix bugs in production and fixed Jenkins script for DevOps",
          "Various tasks in monitoring live application"
        ],
        "technologies": [
          "Git", "JAVA", "Eclipse", "Jenkins", "JUnit", "PowerMock", "Kibana", "Bitbucket", "Liquibase"
        ],
        "position": "Full-Stack JAVA Developer",
        "startDate": "2019-07-01",
        "summary": "Ordering plattform for automotive customer",
        "url": "https://capgemini.com"
      },
      {
        "name": "Capgemini",
        "endDate": "2019-06-30",
        "highlights": [
          "Refactoring existing JAVA MVC Application into a more modern two tier solution",
          "Backend JAVA Spring Boot, Frontend Dojo + Typescript (JS Framework)",
          "Fast onboarding (development and testing of production-ready component within first two days)",
          "Part of distributed SCRUM Team: 2 on-shore colleagues, ~8 off-shore. Dailies, 2 week sprints",
          "Task Tracking with Atlassian Software (JIRA, Confluence)"
        ],
        "technologies": [
          "Git", "JAVA Spring", "Typescript", "Dojo", "JIRA", "Jest Testing", "SASS", "Selenium Webdriver"
        ],
        "summary": "Tool to configure complex SQL queries (AND/OR) over multiple databases in one user interface",
        "position": "Frontend Developer",
        "startDate": "2019-02-01",
        "url": "https://capgemini.com"
      },
      {
        "name": "Capgemini",
        "endDate": "2019-01-31",
        "highlights": [
            "Concept to rewrite a MS Access Data solution into JAVA with main solution architect",
            "Analysis of the existing architecture and creation of a new high level layout for microservices",
            "Programming of Windows installer in C#",
            "ETL Task: Hot/Cold Analysis of COBOL Code through Batch script and visualisation with pivot table for upper management. Preprocessing and aggregation was done with BigQuery",
            "Doing code reviews, writing Unit Tests"
        ],
        "technologies": [
          "Git", "GCP BigQuery", "Excel", "C#", "Batch-scripting", "JAVA", "Microservicing", "Windows"
        ],
        "position": "Software Engineer",
        "startDate": "2018-09-01",
        "summary": "Global Ordering platform for automotive company and several small tasks",
        "url": "https://capgemini.com"
      },
      {
        "name": "Bosch Engineering GmbH",
        "endDate": "2018-08-31",
        "highlights": [
            "Development of two containerised applications running on MERN Stack (MongoDb, Node.js, React, Express)",
            "Running of containers in Docker Swarm, then Kubernetes Cluster",
            "Inside loose SCRUM Team as Developer, Weekly reports monthly retrospective",
            "Continuous, close coordination with the client",
            "Writing Unit Tests"
        ],
        "technologies": [
          "Git", "Kubernetes", "Docker", "Docker CLI",  "React", "MongoDb", "Node.js", "JIRA", "Slack", "Jasmine, Karma Tests"
        ],
        "position": "Full-Stack MERN Developer",
        "startDate": "2018-01-31",
        "summary": "Full stack development tasks for a fleet management platform",
        "url": "https://bosch-engineering.com"
      },
      {
        "name": "Daimler AG (Vans)",
        "endDate": "2017-12-31",
        "highlights": [
          "ETL Pipelines from different Datasources, gathered into single excel sheet",
          "Operational & Strategical Planning tool"
        ],
        "technologies": [
          "SQL", "Excel", "VBA", "Powerpoint"
        ],
        "position": ".NET Developer",
        "startDate": "2017-09-30",
        "summary": "Development of a VBA-based Excel Tool to display and compare take-rates of different sources",
        "url": "https://daimler.com/produkte/transporter/"
      }
    ],
    "volunteer": [],
    "education": [
      {
        "url": "https://www.uni-stuttgart.de/",
        "institution": "University of Stuttgart",
        "area": "Technology Management",
        "studyType": "Master of Science",
        "startDate": "2017-12-31",
        "endDate": "2020-12-31",
        "gpa": "2.3",
        "courses": [
          "Master thesis: A methodology to determine flexibility of service-oriented embedded architectures",
          "Student thesis: Service evolution of service-oriented, embedded architectures",
          "GPA: 2.3"
        ],
        "technologies": ["Git", "C++", "GCP Compute", "Python", "Anaconda", "Jupyther"]
      },
      {
        "url": "https://www.uni-stuttgart.de/",
        "institution": "University of Stuttgart",
        "area": "Technology Management",
        "studyType": "Bachelor of Science",
        "startDate": "2013-12-31",
        "endDate": "2017-12-31",
        "gpa": "2.1",
        "courses": [
          "Bachelor thesis in a agent-based simulation model(JAVA) of buying decisions on a closed market (renewable energy)",
          "GPA: 2.1, Final thesis: 1.0"
        ],
        "technologies": ["Git", "JAVA", "Eclipse", "Matlab"]
      }
    ],
    "awards": [
      {
        "title": "Certified Kubernetes Administrator",
        "date": "2020-11-01",
        "awarder": "The Linux Foundation",
        "summary": "",
        "url": "https://www.youracclaim.com/badges/fa2bba17-db37-4902-86ae-0d4b735aa9c1/public_url"
    },
      {
      "title": "GCP Associate Cloud Engineer",
      "date": "2020-08-01",
      "awarder": "Google Cloud Platform",
      "summary": "",
      "url": ""
  },
  {
      "title": "AWS Certified Developer – Associate",
      "date": "2020-09-01",
      "awarder": "Amazon Web Services",
      "summary": "",
      "url": ""
  },
  {
      "title": "AWS Certified Cloud Practitioner",
      "date": "2020-06-01",
      "awarder": "Amazon Web Services",
      "summary": "",
      "url": ""
  },
  {
      "title": "AWS Solutions Architect",
      "date": "2020-06-01",
      "awarder": "Amazon Web Services",
      "summary": "",
      "url": ""
  },
  {
      "title": "AWS Technical Professional",
      "date": "2020-06-01",
      "awarder": "Amazon Web Services",
      "summary": "",
      "url": ""
  },
  {
      "title": "AWS Business Professional",
      "date": "2020-06-01",
      "awarder": "Amazon Web Services",
      "summary": "",
      "url": ""
  }],
    "publications": [
      {
        "name": "IaC Solutions for AWS Cloud 2020: which one is the right for you?",
        "releaseDate": "2020-10-30",
        "publisher": "Julian de Mourgues",
        "summary": "A short article comparing IaC Frameworks for AWS",
        "website": "https://juliandm.medium.com/iac-solutions-for-aws-cloud-2020-which-one-is-the-right-for-you-365f8411b28b"
      }
    ],
    "skills": [
        {
            "name": "Communication",
            "level": 70,
            "keywords": [
                "(Virtual) Presentation skills",
                "Spontaneous interactions",
                "Slack",
                "Microsoft Teams"
            ]
        },
      {
        "name": "Project Collaboration & Management",
        "level": 80,
        "keywords": [
            "Leading skills",
            "Product Owner for several freelance projects",
            "Specification sheets",
            "SCRUM/KANBAN",
            "Jira Board"
        ]
      },
      {
        "name": "Databases",
        "level": 60,
        "keywords": [
            "DynamoDB",
            "GCP BigQuery",
            "MongoDB",
            "SQLite",
            "Liquibase",
            "Relational Modelling", "Semantic Modelling", "Entity-Relationship Modelling", "Hierachical Modelling", "Data-Vault Modelling"
        ]
      },
      {
        "name": "FaaS / On-Demand Computing",
        "level": 80,
        "keywords": [
            "EC2",
            "AWS: Lambda",
            "Cloud Run"
        ]
      },
      {
        "name": "Container Orchestration",
        "level": 45,
        "keywords": [
            "AWS Fargate & GCP Cloud Run",
            "Docker, ECS",
            "Kubernetes",
            "AWS SQS", "GCP Pub/Sub",
            "Kafka"
        ]
      },
      {
        "name": "Cloud Security & Compliance",
        "level": 65,
        "keywords": [
            "IAM (AWS, GCP)",
            "VPC Security (Shield, Security Groups, ACLs)",
            "CloudWatch", "CloudTrail", "Logs"
        ]
      },
      {
        "name": "Logging, Debugging, Testing",
        "level": 70,
        "keywords": [
            "ELK Stack (Kibana)",
            "JUnit", "Mockito", "PowerMock", "Jest",
            "Test Automation",
            "Local debugging",
            "Jasmine & Karma for React",
            "AWS XRay", "CloudWatch", "CloudTrail"
        ]
      },
      {
        "name": "Kubernetes",
        "level": 60,
        "keywords": [
          "commands with kubectl",
          "Logging (Prometheus)",
          "CNI (Calico, Weaveworks)"
        ]
      },
      {
        "name": "Package/Dependency management",
        "level": 90,
        "keywords": [
          "Java: Maven",
          "Node/Js: Npm, Yarn",
          "Python: Pip, Pipenv, Anaconda",
          "C++: CMake"
        ]
      },
      {
        "name": "IDEs & Code Formatting",
        "level": 85,
        "keywords": [
          "SonarQube, Prettify, EsLint",
          "JAVA: JUnit, Javascript: Jest",
          "IntelliJ (WebSphere/PyCharm)", "Visual Studio Code", "Cloud9", "Eclipse"
        ]
      },
      {
        "name": "Batch processes",
        "level": 55,
        "keywords": [
            "GCP: Cloud Scheduler + Functions + EC2 with job on container",
            "Cmder, Shell Scripting"
        ]
      },
      {
        "name": "Hardware, OS & Kernel",
        "level": 45,
        "keywords": [
            "Linux Ubuntu",
            "Linux Alpine",
            "Windows",
            "Raspbian"
        ]
      },
      {
        "name": "Infrastructure as Code (IaC)",
        "level": 60,
        "keywords": [
            "AWS Amplify",
            "AWS Serverless Architecture Model (SAM)",
            "AWS Cloud Development Kit (CDK)",
            "Terraform"
        ]
      },
      {
        "name": "CI / CD",
        "level": 75,
        "keywords": [
          "Jenkins",
          "AWS CodeDeploy", "CodeBuild", "CodeCommit", "CodePipeline",
          "GCP Build",
          "Gitlab/AWS/GCP Pipelines"
        ]
      },
      {
        "name": "Hosting",
        "level": 75,
        "keywords": [
            "AWS CloudFront",
            "API Gateway",
            "AWS Route53"
        ]
      },
      {
        "name": "Security & Best Practices",
        "level": 85,
        "keywords": [
            "No plain text Pws", "Using hashed secrets in k8s",
            "Regular and descriptive commits",
            "GDPR conformity",
            "accessible web applications"
        ]
      },
      {
        "name": "Python",
        "level": 75,
        "keywords": [
            "Anaconda",
            "Pip",
            "Jupyter",
            "Pandas",
            "spaCy (NLP)"
        ]
      },
      {
        "name": "Development Tools",
        "level": 80,
        "keywords": [
            "Git CLI",
            "Git Branching",
            "Bitbucket", "Github", "GitLab"
        ]
      },
      {
        "name": "React",
        "level": 95,
        "keywords": [
            "Redux",
            "Sagas",
            "Hooks",
            "Material UI",
            "Reselect",
            "React Native"
        ]
      },
      {
        "name": "JAVA",
        "level": 70,
        "keywords": [
            "JUnit Tests", "Mockito",  "PowerMock",
            "Spring Boot",
            "JSF/JRE",
            "Tomcat/JBoss"
        ]
      },
      {
        "name": "C++",
        "level": 40,
        "keywords": [
            "Realtime Programming",
            "Cmake",
            "GnuPlot"
        ]
      },
      {
        "name": "Node.js",
        "level": 95,
        "keywords": [
            "GraphQL",
            "Express",
            "Typescript",
            "Axios",
            "Testing: Jest"
        ]
      }
    ],
    "languages": [
      {
        "language": "de",
        "fluency": "Native Speaker"
      },
      {
        "language": "en",
        "fluency": "Fluent, TOEFL 101/120"
      },
      {
        "language": "fr",
        "fluency": "Good enough for casual talking"
      },
      {
        "language": "es",
        "fluency": "Enough to order in a restaurant"
      },
      {
        "language": "tr",
        "fluency": "Self introduction and basic small talk"
      }
    ],
    "interests": [
    {
      "name": "☁️",
      "keywords": [
        "Cloud Technologies",
        "Cloud Startups",
        "Open Source Community",
        "Certification Marathon"
      ]
    },
    {
      "name": "🧠",
      "keywords": [
        "Guitar",
        "Dance Music Production",
        "Digital Art",
        "Poetry"
      ]
    }
    ],
    "references": []
  }