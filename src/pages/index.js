import React, { createRef, memo, useMemo, useRef } from "react"
import styled from "styled-components"
import { Waypoint } from "react-waypoint"
import CssBaseline from "@material-ui/core/CssBaseline"

import SEO from "../components/seo"
import Header from "../components/header"
import Footer from "../components/footer"
import ScrollTop from "../components/scrolltop"
import Drawer from "../components/drawer"
import sections from "../sections"

import useDrawerCtrl from "../util/drawerctrl"
import useScrollCtrl from "../util/scrollctrl"
import useTabCtrl from "../util/tabctrl"

const Wrapper = styled.div`

`
const Section = memo(
  ({ id, i, updateVis, forwardedRef, component, forwardedProps }) => (
    <Waypoint
      onEnter={() => updateVis(i, true)}
      onLeave={() => updateVis(i, false)}
      bottomOffset="25%"
    >
      <section id={id} ref={forwardedRef}>
        {React.cloneElement(component, forwardedProps[i])}
      </section>
    </Waypoint>
  )
)

const IndexPage = (props) => {
  const refs = useRef(sections.map(() => createRef()))
  const { trigger } = useScrollCtrl()
  const { drawer, toggleDrawer } = useDrawerCtrl()
  const { tab, updateTab, updateVis } = useTabCtrl(refs)
  const forwardedProps = useMemo(() => [{ updateTab }, {}, {}], [])

  return (
    <Wrapper>
      {/* <SEO
        title={"Test"}
      /> */}
      <CssBaseline />
      <Header
        tab={tab}
        updateTab={updateTab}
        sections={sections}
        trigger={trigger}
        toggleDrawer={toggleDrawer}
        dark={props.dark}
        toggleDark={props.toggleDark}
        toggleLan={props.toggleLan}
        isGerman={props.isGerman}
      />
      {sections.map(({ id, component }, i) => (
        <Section
          id={id}
          key={id}
          i={i}
          updateVis={updateVis}
          forwardedRef={refs.current[i]}
          component={component}
          forwardedProps={forwardedProps}
        />
      ))}
      <Footer />
      <Drawer
        tab={tab}
        updateTab={updateTab}
        sections={sections}
        drawer={drawer}
        toggleDrawer={toggleDrawer}
        dark={props.dark}
        toggleDark={props.toggleDark}
        toggleLan={props.toggleLan}
        isGerman={props.isGerman}
      />
      <ScrollTop updateTab={updateTab} trigger={trigger} />
    </Wrapper>
  )
}

export default IndexPage
