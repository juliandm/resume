import { useCallback, useState } from "react"

export default () => {
  const [isGerman, setGerman] = useState(false)
  const toggleGerman = useCallback(() => setGerman((d) => !d), [])
  return { isGerman, toggleGerman }
}
